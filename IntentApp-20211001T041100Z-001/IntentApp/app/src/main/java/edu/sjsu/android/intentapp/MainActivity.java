package edu.sjsu.android.intentapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final int INTENT_GET_MSG= 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button broserButton = (Button) findViewById(R.id.button);
        broserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uriString = "http://www.amazon.com";
                String title = getResources().getString(R.string.chooser_title);
                Intent webActivity = new Intent(Intent.ACTION_VIEW, Uri.parse(uriString));
                Intent chooser = Intent.createChooser(webActivity, title);
                startActivity(chooser);
            }
        });

        Button makeCalls = (Button) findViewById(R.id.button2);
        makeCalls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = "tel: +19491234444";
                Intent callActivity = new Intent(Intent.ACTION_DIAL, Uri.parse(number));
                startActivity(callActivity);
            }
        });
    }

}